# geosymmetric.js

A script for setting random background on HTML element. Generated background consists of randomly placed, sized, shaped and colored figures on rotated and "mirrored" canvas.

![geosymmetric.js examples](screenshot.png)

## Name

"Geosymmetric" is a word made up from "geometric" and "symmetric".

## How to use

1. Add [randomColor](https://github.com/davidmerfield/randomColor) to your project.
1. Add src/geosymmetric.js to your project.
1. Set `data-geosymmetric` attribute on the element you wish to have background generated, and you're done!

## Options

All parameters are set declaratively through `data-*` attributes.

| Attribute | Value type | Default value | Description |
| --------- | ---------- | ------------- | ----------- |
| `data-geosymmetric` | none | none | **Required** Marks HTML element to have background generated. |
| `data-geosymmetric-axis` | String | `horizontal` | How the background will be mirrored. Possible values: `horizontal`, `vertical`. |
| `data-geosymmetric-color-dark` | String | [randomColor()](https://github.com/davidmerfield/randomColor) | Base color, set it to color hex code (ie. `#ff0000`) or let it be random, pretty one. |
| `data-geosymmetric-elems-min` | Number | `3` | Minimum number (inclusive) of generated shapes on each half of background. |
| `data-geosymmetric-elems-max` | Number | `8` | Maximum number (inclusive) of generated shapes on each half of background. |
| `data-geosymmetric-rotation-max` | Number | `15` | Maximum number of degrees (exclusive) of rotation applied to canvas. |

## TODO

- [ ] Wrap in JS module and publish on npm/bower (randomColor as npm dependency)
- [ ] Support multiple `[data-geosymmetric]` elements
- [ ] Add more shapes to render (rhombus, hexagon etc.)
- [ ] Rename elems -> shapes
- [ ] Make `rotationMax` inclusive (so `0` can be set to disable rotation)
- [ ] Add console.warning (and continue) if couldn't set positioning context
- [ ] Add check for `elemsMin > elemsMax`
- [ ] Add console.warning (and continue) if `container` already has background
- [ ] Return if element width or height == 0
