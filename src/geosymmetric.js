(function() {

  var container = document.querySelector('[data-geosymmetric]');

  if (!container) {
    return;
  }

  if (typeof window.randomColor !== 'function') {
    return console.error('geosymmetric.js requires randomColor, which was not found.');
  }

  var containerStyles = window.getComputedStyle(container);

  if (containerStyles['transform'] === 'none') {
    container.style.transform = 'scale(1)';
  }
  else if (containerStyles['position'] === 'static') {
    container.style.position = 'relative';
  }

  function getColorLight(colorDark) {
    return colorDark
      .split('')
      .map(function(digit) {
        if (digit === '#') {
          return digit;
        }
        return Math.min(parseInt(digit, 16) + 4, 15).toString(16);
      })
      .join('');
  }

  function randInt(maxExclusive) {
    return Math.floor(Math.random() * maxExclusive);
  }

  function randElem(array) {
    return array[randInt(array.length)];
  }

  var config = (function() {
    var colorDark = container.dataset.geosymmetricColorDark || randomColor();

    return {
      axis: container.dataset.geosymmetricAxis || 'horizontal',
      colorDark: colorDark,
      colorLight: container.dataset.geosymmetricColorLight || getColorLight(colorDark),
      elemsMin: container.dataset.geosymmetricElemsMin || 3,
      elemsMax: container.dataset.geosymmetricElemsMax || 8,
      rotationMax: container.dataset.geosymmetricRotationMax || 15,
    };
  }());

  var containerWidth = container.offsetWidth;
  var containerHeight = container.offsetHeight;

  var canvasSize = {
    horizontal: {
      width: [
        Math.ceil(containerWidth / 2),
        Math.floor(containerWidth / 2)
      ],
      height: [containerHeight, containerHeight]
    },
    vertical: {
      width: [containerWidth, containerWidth],
      height: [
        Math.ceil(containerHeight / 2),
        Math.floor(containerHeight / 2)
      ]
    }
  };
  var canvasPosition = {
    horizontal: [
      ['top', 'left'],
      ['top', 'right'],
    ],
    vertical: [
      ['top', 'left'],
      ['bottom', 'left'],
    ]
  };
  var canvasTransform = {
    horizontal: 'scaleX(-1)',
    vertical: 'scaleY(-1)',
  };

  var canvases = [];
  var ctxes = [];
  var ctxRotation = -randInt(config.rotationMax) * Math.PI / 180;

  for (var i = 0; i < 2; i++) {
    var canvas = document.createElement('canvas');
    canvas.width = canvasSize[config.axis].width[i];
    canvas.height = canvasSize[config.axis].height[i];
    canvas.style.opacity = '0';
    canvas.style.position = 'absolute';
    canvasPosition[config.axis][i].forEach(function(direction) {
      canvas.style[direction] = 0;
    });
    if (i === 1) {
      canvas.style.transform = canvasTransform[config.axis];
    }
    container.appendChild(canvas);
    canvases.push(canvas);

    var ctx = canvas.getContext('2d');
    ctx.rotate(ctxRotation);
    ctxes.push(ctx);
  };

  var elemSizes = [0.1, 0.2, 0.4];
  var elemShapes = ['circle', 'triangle', 'square'];

  function getElemParams() {
    return {
      size: randElem(elemSizes) * containerWidth,
      shape: randElem(elemShapes),
      center: {
        x: randInt(canvasSize[config.axis].width[0]),
        y: randInt(canvasSize[config.axis].height[0]),
      },
    };
  }

  function drawElem(ctx, elem) {
    var elemGradient = ctx.createLinearGradient(
      0, elem.center.y - 0.5 * elem.size,
      0, elem.center.y + 0.5 * elem.size
    );
    elemGradient.addColorStop(0, config.colorDark);
    elemGradient.addColorStop(1, config.colorLight);
    ctx.fillStyle = elemGradient;

    switch (elem.shape) {
      case 'circle':
        ctx.beginPath();
        ctx.arc(
          elem.center.x,
          elem.center.y,
          0.5 * elem.size,
          0,
          2 * Math.PI
        );
        ctx.closePath();
        ctx.fill();
        break;
      case 'square':
        ctx.fillRect(
          elem.center.x - 0.5 * elem.size,
          elem.center.y - 0.5 * elem.size,
          elem.size,
          elem.size
        );
        break;
      case 'triangle':
        ctx.beginPath();
        ctx.moveTo(
          elem.center.x + 0.5 * elem.size * Math.cos(1 / 6 * Math.PI),
          elem.center.y + 0.5 * elem.size * Math.sin(1 / 6 * Math.PI)
        );
        ctx.lineTo(
          elem.center.x + 0.5 * elem.size * Math.cos(5 / 6 * Math.PI),
          elem.center.y + 0.5 * elem.size * Math.sin(5 / 6 * Math.PI)
        );
        ctx.lineTo(
          elem.center.x + 0.5 * elem.size * Math.cos(9 / 6 * Math.PI),
          elem.center.y + 0.5 * elem.size * Math.sin(9 / 6 * Math.PI)
        );
        ctx.closePath();
        ctx.fill();
        break;
    }
  }

  container.style.background =
    'linear-gradient(to top, ' +
    config.colorDark + ', ' +
    config.colorLight + ')';

  var elemNumber = config.elemsMin + randInt(config.elemsMax - config.elemsMin + 1);

  for (var i = 0; i < elemNumber; i++) {
    var elem = getElemParams();
    drawElem(ctxes[0], elem);
  }

  ctxes[1].putImageData(
    ctxes[0].getImageData(
      config.axis === 'horizontal' && canvases[0].width > canvases[1].width ? 1 : 0,
      config.axis === 'vertical' && canvases[0].height > canvases[1].height ? 1 : 0,
      canvases[1].width,
      canvases[1].height
    ),
    0,
    0
  );

  canvases.forEach(function(canvas) {
    canvas.style.opacity = '1';
  });

}());
